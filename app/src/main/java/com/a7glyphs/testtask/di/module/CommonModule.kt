package com.a7glyphs.testtask.di.module

import android.content.Context
import com.a7glyphs.testtask.mvp.p.Router
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CommonModule(private val context: Context) {

    @Provides
    @Singleton
    internal fun provideContext(): Context {
        return context
    }

    @Provides
    @Singleton
    internal fun provideRouter(context: Context): Router {
        return Router(context)
    }
}
