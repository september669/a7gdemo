package com.a7glyphs.testtask.di.module

import com.a7glyphs.testtask.mvp.m.ColorModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModelModule {

    @Provides
    @Singleton
    internal fun provideSelectedColor(): ColorModel {
        return ColorModel()
    }

}
