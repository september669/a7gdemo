package com.a7glyphs.testtask.di

import com.a7glyphs.testtask.App
import com.a7glyphs.testtask.di.module.CommonModule
import com.a7glyphs.testtask.di.module.DaoModule
import com.a7glyphs.testtask.di.module.ModelModule
import com.a7glyphs.testtask.di.module.RestApiModule
import com.a7glyphs.testtask.mvp.m.ColorModel
import com.a7glyphs.testtask.mvp.m.net.APIServiceHolder
import com.a7glyphs.testtask.mvp.m.net.TokenAuthenticator
import com.a7glyphs.testtask.mvp.p.ColorSelectPresenter
import com.a7glyphs.testtask.mvp.p.ColorViewPresenter
import com.a7glyphs.testtask.mvp.p.LoginPresenter
import com.a7glyphs.testtask.mvp.p.Router
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(RestApiModule::class, CommonModule::class, DaoModule::class, ModelModule::class))
interface AppComponent {

    fun inject(app: App)
    fun inject(colorModel: ColorModel)
    fun inject(loginPresenter: LoginPresenter)
    fun inject(tokenAuthenticator: TokenAuthenticator)
    fun inject(apiServiceHolder: APIServiceHolder)
    fun inject(router: Router)
    fun inject(colorSelectPresenter: ColorSelectPresenter)
    fun inject(colorViewPresenter: ColorViewPresenter)

}
