package com.a7glyphs.testtask.di.module

import com.a7glyphs.testtask.mvp.m.net.APIServiceHolder
import com.a7glyphs.testtask.mvp.m.net.Api7gDemo
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = arrayOf(RetrofitModule::class))
class RestApiModule {

    @Provides
    @Singleton
    fun getApi7gDemo(retrofit: Retrofit, serviceHolder: APIServiceHolder): Api7gDemo {
        val api = retrofit.create<Api7gDemo>(Api7gDemo::class.java)
        serviceHolder.api = api
        return api
    }
}
