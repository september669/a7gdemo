package com.a7glyphs.testtask.di.module

import android.content.Context
import com.a7glyphs.testtask.mvp.m.db.Dao
import com.a7glyphs.testtask.mvp.m.db.data.MyObjectBox
import dagger.Module
import dagger.Provides
import io.objectbox.BoxStore
import javax.inject.Singleton

@Module(includes = arrayOf(CommonModule::class))
class DaoModule {

    @Provides
    @Singleton
    internal fun provideDaoSession(context: Context): BoxStore {
        return MyObjectBox.builder().androidContext(context).build()
    }

    @Provides
    @Singleton
    internal fun provideDao(boxStore: BoxStore): Dao {
        return Dao(boxStore)
    }

}