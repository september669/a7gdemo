package com.a7glyphs.testtask.di.module

import com.a7glyphs.testtask.mvp.m.net.APIServiceHolder
import com.a7glyphs.testtask.mvp.m.net.Api7gDemo
import com.a7glyphs.testtask.mvp.m.net.Credential
import com.a7glyphs.testtask.mvp.m.net.TokenAuthenticator
import com.github.aurae.retrofit2.LoganSquareConverterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = arrayOf(CommonModule::class))
class RetrofitModule {

    @Provides
    @Singleton
    internal fun provideRetrofitBuilder(okHttpClient: OkHttpClient): Retrofit.Builder {

        if (Api7gDemo.API_BASE_URL.length < 3) {
            throw RuntimeException("Can't load API_BASE_URL")
        }
        return Retrofit.Builder()
                .baseUrl(Api7gDemo.API_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(LoganSquareConverterFactory.create())
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(builder: Retrofit.Builder): Retrofit {
        return builder.build()
    }

    @Provides
    @Singleton
    internal fun provideCredential(): Credential {
        return Credential()
    }

    @Provides
    @Singleton
    internal fun provideAPIServiceHolder(): APIServiceHolder {
        return APIServiceHolder()
    }

    @Provides
    @Singleton
    internal fun provideAuthenticator(): TokenAuthenticator {
        return TokenAuthenticator()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(authenticator: TokenAuthenticator): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY



        return OkHttpClient()
                .newBuilder()
                .addInterceptor(logging)
                .addInterceptor(authenticator)
                .authenticator(authenticator)
                .build()
    }
}
