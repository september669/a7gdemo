package com.a7glyphs.testtask

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.os.Handler
import android.os.Looper
import android.support.annotation.ColorInt
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.util.*

fun runUi(block: () -> Unit, delayMillis: Long = 0) {
    Handler(Looper.getMainLooper()).postDelayed({ block() }, delayMillis)
}

fun EditText.afterTextChanged(afterTextChanged: (String?) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable?.toString())
        }
    })
}

fun Date.add(field: Int, amount: Int): Date {
    val cal = Calendar.getInstance()
    cal.time = this
    cal.add(field, amount)
    return cal.time
}

fun Date.truncTime(): Date {
    val cal = Calendar.getInstance() // locale-specific
    cal.time = this
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    return cal.time
}

val Int.dp: Int get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.px: Int get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Int.toStringColor(): String = String.format("#%06X", 0xFFFFFF and this)

fun Int.toHsv(): FloatArray {
    val res = FloatArray(3)
    Color.colorToHSV(this, res)
    return res
}

@ColorInt
fun Int.colorFromId(context: Context): Int = ContextCompat.getColor(context, this)
