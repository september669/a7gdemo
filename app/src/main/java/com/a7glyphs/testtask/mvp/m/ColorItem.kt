package com.a7glyphs.testtask.mvp.m

import android.support.annotation.ColorInt
import com.a7glyphs.testtask.mvp.m.db.data.DaoColor
import com.a7glyphs.testtask.truncTime
import java.util.*

interface ColorItem {

    @ColorInt
    fun colorValue(): Int

    fun dateValue(): Date?

    fun stringHexValue(): String

    fun nameValue(): String

    fun createDao(date: Date): DaoColor {
        val intValue = colorValue()
        val name = nameValue()
        return DaoColor(date = date.truncTime(), color = intValue, name = name)
    }


}