package com.a7glyphs.testtask.mvp.m

sealed class Res<out TOk, out TEr : Any> {
    /**
     * Successful result
     */
    class Ok<out TOk, out TEr : Any>(val value: TOk) : Res<TOk, TEr>() {
        override fun toString() = "Result.Ok{value=$value}"
    }

    /**
     * Error result
     */
    class Error<out TOk, out TErr : Any>(val value: TErr) : Res<TOk, TErr>() {
        override fun toString() = "Result.Error{value=$value}"
    }
}


sealed class NetError(val message: String) {

    class NetworkError(message: String) : NetError(message)
    class HttpError(message: String) : NetError(message)

    override fun toString() = "Result.Ok{value=$message}"
}