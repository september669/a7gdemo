package com.a7glyphs.testtask.mvp.m.net.data

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class SrvAuth {

    constructor()

    constructor(password: String?, grantType: String?, scope: String?, clientSecret: String?, clientId: String?, username: String?) {
        this.password = password
        this.grantType = grantType
        this.scope = scope
        this.clientSecret = clientSecret
        this.clientId = clientId
        this.username = username
    }

    @JsonField(name = arrayOf("password"))
    var password: String? = null

    @JsonField(name = arrayOf("grant_type"))
    var grantType: String? = null

    @JsonField(name = arrayOf("scope"))
    var scope: String? = null

    @JsonField(name = arrayOf("client_secret"))
    var clientSecret: String? = null

    @JsonField(name = arrayOf("client_id"))
    var clientId: String? = null

    @JsonField(name = arrayOf("username"))
    var username: String? = null
}