package com.a7glyphs.testtask.mvp.m.db.data

import android.support.annotation.ColorInt
import com.a7glyphs.testtask.mvp.m.ColorItem
import com.a7glyphs.testtask.toStringColor
import com.a7glyphs.testtask.truncTime
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.annotation.Index
import java.util.*

@Entity
class DaoColor : ColorItem {

    constructor()

    constructor(id: Long = 0, date: Date, color: Int, name: String) {
        this.id = id
        this.date = date
        this.color = color
        this.name = name
    }


    @Id
    var id: Long = 0

    @Index
    var date: Date = Date().truncTime()
        set(value) {
            field = value.truncTime()
        }

    @ColorInt
    var color: Int = 0

    var name: String = ""


    override fun stringHexValue() = color.toStringColor()

    override fun colorValue(): Int = color

    override fun dateValue(): Date? = date

    override fun nameValue(): String = name
}