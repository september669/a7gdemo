package com.a7glyphs.testtask.mvp.m.db

import android.graphics.Color
import com.a7glyphs.testtask.add
import com.a7glyphs.testtask.mvp.m.db.data.DaoColor
import com.a7glyphs.testtask.mvp.m.db.data.DaoColor_
import com.a7glyphs.testtask.truncTime
import io.objectbox.BoxStore
import io.objectbox.TxCallback
import io.objectbox.exception.DbException
import io.objectbox.kotlin.boxFor
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.*
import java.util.Calendar.DAY_OF_MONTH
import java.util.Calendar.MILLISECOND
import java.util.concurrent.Callable
import kotlin.coroutines.experimental.suspendCoroutine

class Dao(private val boxStore: BoxStore) : AnkoLogger {

    private val colorBox by lazy {
        boxStore.boxFor(DaoColor::class)
    }
    private val queryColorByDate by lazy {
        colorBox.query().less(DaoColor_.date, Date()).orderDesc(DaoColor_.date).build()
    }

    fun generateTestData(date0: Date, date1: Date) {
        var date = date0
        var i = Color.RED
        while (date < date1) {
            val daoColor = DaoColor(date = date, color = i, name = "Test name")
            colorBox.put(daoColor)
            i += 300
            date = date.add(DAY_OF_MONTH, 1)
        }
    }

    fun removeAll() {
        colorBox.removeAll()
    }

    suspend fun save(daoColor: DaoColor) {
        info("save ${daoColor.date}")
        suspendTxRun(task = Runnable {
            val dateFrom = daoColor.date.truncTime()
            val dateTo = daoColor.date.truncTime().add(DAY_OF_MONTH, 1).add(MILLISECOND, -1)
            val list = colorBox.query().between(DaoColor_.date, dateFrom, dateTo).build().find()
            colorBox.remove(list)
            colorBox.put(daoColor)
        })
    }

    suspend fun getLessDesc(date: Date, offset: Long, limit: Long): MutableList<DaoColor> {
        info("getLess date: $date")
        return suspendTxCall(task = Callable<MutableList<DaoColor>> {
            colorBox.query().less(DaoColor_.date, date).orderDesc(DaoColor_.date).build().find(offset, limit)
            //TODO dda: uncomment when fixed https://github.com/objectbox/objectbox-java/issues/277
            //queryColorByDate.setParameter(DaoColor_.date, date.time).find()
        })
    }

    suspend fun <R> suspendTxCall(task: Callable<R>): R {
        return suspendCoroutine { cont ->
            boxStore.callInTxAsync(task, TxCallback { result: R?, throwable: Throwable? ->
                when {
                    throwable != null -> cont.resumeWithException(throwable)
                    result != null -> cont.resume(result)
                    else -> cont.resumeWithException(DbException("Call returns nothing"))
                }
            })
        }
    }

    suspend fun suspendTxRun(task: Runnable) {
        return suspendCoroutine { cont ->
            boxStore.runInTxAsync(task, TxCallback { _: Void?, throwable: Throwable? ->
                when {
                    throwable != null -> cont.resumeWithException(throwable)
                    else -> cont.resume(Unit)
                }
            })
        }
    }
}