package com.a7glyphs.testtask.mvp.v

import android.content.Intent
import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface BaseView : MvpView {

    fun showProgress(visible: Boolean)


    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(@StringRes string: Int) {
    }

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun runNextView(intent: Intent)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun runNextViewForResult(intent: Intent)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun backToPreviousView()
}