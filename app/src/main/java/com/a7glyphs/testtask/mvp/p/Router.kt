package com.a7glyphs.testtask.mvp.p

import android.content.Context
import android.content.Intent
import com.a7glyphs.testtask.mvp.v.BaseView
import com.a7glyphs.testtask.ui.ColorSelectActivity
import com.a7glyphs.testtask.ui.ColorViewActivity

class Router(val context: Context) {


    fun onLogin(currentView: BaseView) {
        val intent = Intent(context, ColorSelectActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        currentView.runNextView(intent)
    }

    fun onNewColorView(currentView: BaseView) {
        val intent = Intent(context, ColorViewActivity::class.java)
        currentView.runNextViewForResult(intent)
    }

    fun onColorViewOk(currentView: BaseView) {
        currentView.backToPreviousView()
    }

    fun onColorViewCancel(currentView: BaseView) {
        currentView.backToPreviousView()
    }

    fun onColorViewFail(currentView: BaseView) {
        val intent = Intent(context, ColorSelectActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        currentView.runNextView(intent)
    }
}