package com.a7glyphs.testtask.mvp.v

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface LoginView : BaseView {

    fun switchLoginPossible(isPossible: Boolean)

}