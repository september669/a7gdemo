package com.a7glyphs.testtask.mvp.m.net.data

import android.graphics.Color
import com.a7glyphs.testtask.mvp.m.ColorItem
import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonIgnore
import com.bluelinelabs.logansquare.annotation.JsonObject
import com.bluelinelabs.logansquare.annotation.OnJsonParseComplete

@JsonObject
class SrvColor : ColorItem {

    @JsonField(name = arrayOf("name"))
    var name: String? = null

    @JsonField(name = arrayOf("hex"))
    var hexStringValue: String? = null

    @JsonIgnore
    var intValue: Int = Int.MIN_VALUE

    @JsonIgnore
    var isValid: Boolean = false

    @OnJsonParseComplete
    fun validate() {
        try {
            intValue = Color.parseColor(hexStringValue)
        } catch (e: Exception) {
        }
        isValid = !name.isNullOrEmpty() && !hexStringValue.isNullOrEmpty() && intValue != Int.MIN_VALUE
        //info("isValid: $isValid, name: $name, hexStringValue: $hexStringValue,  intValue: $intValue")
    }

    override fun nameValue() = name.orEmpty()

    override fun stringHexValue() = hexStringValue.orEmpty()

    override fun colorValue() = intValue

    override fun dateValue() = null

}
