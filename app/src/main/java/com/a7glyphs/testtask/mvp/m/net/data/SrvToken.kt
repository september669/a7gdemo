package com.a7glyphs.testtask.mvp.m.net.data

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class SrvToken {

    @JsonField(name = arrayOf("access_token"))
    var accessToken: String? = null

    @JsonField(name = arrayOf("refresh_token"))
    var refreshToken: String? = null

    @JsonField(name = arrayOf("token_type"))
    var tokenType: String? = null

    @JsonField(name = arrayOf("expires_in"))
    var expiresIn: Long? = null

}