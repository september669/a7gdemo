package com.a7glyphs.testtask.mvp.v

import com.a7glyphs.testtask.mvp.m.ColorItem
import com.a7glyphs.testtask.mvp.m.db.data.DaoColor
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import java.util.*

const val SET_TODAY_COLOR_TAG: String = "setTodayColor"

@StateStrategyType(AddToEndSingleStrategy::class)
interface SelectColorView : BaseView {

    fun setHeader(date: Date)

    fun showColorHistory(colors: List<DaoColor>)

    fun addColorHistoryToStart(colors: List<DaoColor>)

    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun setTodayColor(color: ColorItem, comparator: (left: ColorItem?, right: ColorItem?) -> Boolean)

    fun showColors(colors: List<ColorItem>)

    fun showProgressHistory(visible: Boolean)

}