package com.a7glyphs.testtask.mvp.p

import com.a7glyphs.testtask.App
import com.a7glyphs.testtask.mvp.m.ColorModel
import com.a7glyphs.testtask.mvp.m.ColorModel.States.*
import com.a7glyphs.testtask.mvp.m.db.Dao
import com.a7glyphs.testtask.mvp.v.ColorView
import com.arellomobile.mvp.InjectViewState
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import java.util.*
import javax.inject.Inject

@InjectViewState
class ColorViewPresenter : BasePresenter<ColorView>() {
    init {
        App.appComponent.inject(this)
    }

    @Inject
    lateinit var dao: Dao

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var colorModel: ColorModel

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        info("onFirstViewAttach")
        val color = colorModel.color
        if (!colorModel.isEmpty() && color != null) {
            viewState.showColor(color)
        } else {
            router.onColorViewFail(viewState)
        }
    }

    fun onColorCancel() {
        info("onColorCancel")
        colorModel.state = NEW
        router.onColorViewCancel(viewState)
    }

    fun onColorApprove() {
        info("onColorApprove")
        colorModel.state = APPROVED
        val daoColor = colorModel.color?.createDao(Date())
        if (daoColor != null) {
            launch(UI) {
                dao.save(daoColor)
                colorModel.color = daoColor
                colorModel.state = SAVED
                info("onColorApprove color saved")
                router.onColorViewOk(viewState)
            }
        } else {
            error("onColorApprove color save failed")
        }
    }
}