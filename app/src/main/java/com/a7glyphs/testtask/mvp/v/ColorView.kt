package com.a7glyphs.testtask.mvp.v

import com.a7glyphs.testtask.mvp.m.ColorItem
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ColorView : BaseView {

    fun showColor(color: ColorItem)

}