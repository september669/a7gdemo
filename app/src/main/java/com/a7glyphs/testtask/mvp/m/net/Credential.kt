package com.a7glyphs.testtask.mvp.m.net

import com.a7glyphs.testtask.mvp.m.net.data.SrvAuth
import com.a7glyphs.testtask.mvp.m.net.data.SrvToken

class Credential {
    companion object {
        val GRANT_TYPE = "password"
        val SCOPE = "*"
    }


    var userName: String? = null

    var email: String? = null

    var password: String? = "Test123"

    var clientId: String? = "2"

    var clientSecret: String? = "M1m2QgSdbRKWstTaVXlhdPotJ6WCC33rLLq3N6fK"

    var srvToken: SrvToken? = null

    fun isReadyForLogin(): Boolean = !userName.isNullOrEmpty() && !email.isNullOrEmpty()

    fun getAuthData() = SrvAuth(username = email,
            password = password,
            clientId = clientId,
            clientSecret = clientSecret,
            grantType = GRANT_TYPE,
            scope = SCOPE
    )
}