package com.a7glyphs.testtask.mvp.m.net

import com.a7glyphs.testtask.App
import okhttp3.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import javax.inject.Inject

class TokenAuthenticator : Authenticator, Interceptor, AnkoLogger {
    init {
        App.appComponent.inject(this)
    }

    companion object {
        val AUTHORIZATION = "Authorization"
    }

    @Inject
    lateinit var service: APIServiceHolder

    @Inject
    lateinit var credential: Credential

    @Synchronized
    override fun authenticate(route: Route?, response: Response?): Request? {
        // Refresh your access_token using a synchronous api request
        val newTokenCall = service.api.refreshToken(credential.getAuthData())
        val newToken = newTokenCall.execute().body()
        credential.srvToken = newToken
        // Add new header to rejected request and retry it

        val token: String = credential.srvToken?.refreshToken ?: return null

        //Returns null if the challenge cannot be satisfied.

        return response
                ?.request()
                ?.newBuilder()
                ?.header(AUTHORIZATION, "Bearer $token")
                ?.build()
    }

    override fun intercept(chain: Interceptor.Chain): Response {

        val token: String? = credential.srvToken?.accessToken
        info("intercept with token: $token")
        return if (token != null) {
            val original = chain.request()
            val builder = original
                    .newBuilder()
                    .header(AUTHORIZATION, "Bearer $token")
            val request = builder.build()
            chain.proceed(request)
        } else {
            chain.proceed(chain.request())
        }
    }


}

class APIServiceHolder {
    lateinit var api: Api7gDemo
}