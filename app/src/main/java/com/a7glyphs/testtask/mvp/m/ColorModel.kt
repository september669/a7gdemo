package com.a7glyphs.testtask.mvp.m

import com.a7glyphs.testtask.App
import com.a7glyphs.testtask.mvp.m.net.Api7gDemo
import com.a7glyphs.testtask.mvp.m.net.Credential
import io.reactivex.subjects.PublishSubject
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import ru.gildor.coroutines.retrofit.Result
import ru.gildor.coroutines.retrofit.awaitResult
import javax.inject.Inject

class ColorModel : AnkoLogger {
    enum class States {
        NEW,
        APPROVED,
        SAVED
    }

    init {
        App.appComponent.inject(this)
    }


    @Inject
    lateinit var credential: Credential

    @Inject
    lateinit var api7gDemo: Api7gDemo

    var color: ColorItem? = null

    val subjectNewColor = PublishSubject.create<ColorItem>()

    var state: States = States.NEW
        set(value) {
            val tmpColor = color
            if (field != States.SAVED && value == States.SAVED && tmpColor != null) {
                subjectNewColor.onNext(tmpColor)
            }
            field = value
        }

    fun isEmpty() = color == null

    suspend fun refreshToken(): Res<Boolean, NetError> {
        val result = api7gDemo.refreshToken(credential.getAuthData()).awaitResult()
        info("refreshToken: $result")
        when (result) {
            is Result.Ok -> {
                credential.srvToken = result.value
                return Res.Ok(true)
            }
            is Result.Error -> {
                error("onLogin: $result")
                return Res.Error(NetError.HttpError(result.exception.message()))
            }
            is Result.Exception -> {
                error("onLogin: $result", result.exception)
                return Res.Error(NetError.NetworkError(result.exception.message.orEmpty()))
            }
        }

    }


}