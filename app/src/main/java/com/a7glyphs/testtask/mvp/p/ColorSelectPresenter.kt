package com.a7glyphs.testtask.mvp.p

import com.a7glyphs.testtask.App
import com.a7glyphs.testtask.R
import com.a7glyphs.testtask.mvp.m.ColorItem
import com.a7glyphs.testtask.mvp.m.ColorModel
import com.a7glyphs.testtask.mvp.m.ColorModel.States.NEW
import com.a7glyphs.testtask.mvp.m.ColorModel.States.SAVED
import com.a7glyphs.testtask.mvp.m.db.Dao
import com.a7glyphs.testtask.mvp.m.net.Api7gDemo
import com.a7glyphs.testtask.mvp.v.SelectColorView
import com.a7glyphs.testtask.truncTime
import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import ru.gildor.coroutines.retrofit.Result
import ru.gildor.coroutines.retrofit.awaitResult
import java.util.*
import javax.inject.Inject

@InjectViewState
class ColorSelectPresenter : BasePresenter<SelectColorView>() {
    init {
        App.appComponent.inject(this)
    }

    companion object {
        val LOAD_ITEM_PAGE_CNT = 15L
    }

    @Inject
    lateinit var dao: Dao

    @Inject
    lateinit var api7gDemo: Api7gDemo

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var colorModel: ColorModel

    override fun onFirstViewAttach() {
        info("onFirstViewAttach")

        super.onFirstViewAttach()
        val today = Calendar.getInstance().time
        viewState.setHeader(today)

        launch(UI) {
            info("dao.getLess $today")
            viewState.showProgressHistory(true)
            val colors = dao.getLessDesc(today, 0, LOAD_ITEM_PAGE_CNT)
            viewState.showColorHistory(colors)
            viewState.showProgressHistory(false)
        }

        launch(UI) {
            try {
                viewState.showProgress(true)
                info("dao.getColors")
                val result = api7gDemo.getColors().awaitResult()
                delay(10)
                when (result) {
                    is Result.Ok -> viewState.showColors(result.value.filter { it.isValid })
                    is Result.Error -> {
                        error("dao.getColors: $result")
                        viewState.showError(R.string.server_error)
                    }
                    is Result.Exception -> {
                        error("dao.getColors: $result", result.exception)
                        viewState.showError(R.string.network_error)
                    }
                }
            } finally {
                viewState.showProgress(false)
            }
        }

        val disposable = colorModel.subjectNewColor
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .forEach { colorItem ->
                    if (colorModel.state == SAVED && colorItem != null) {
                        viewState.setTodayColor(colorItem,
                                comparator = { l, r ->
                                    l?.dateValue()?.truncTime() != null && l.dateValue()?.truncTime() == r?.dateValue()?.truncTime()
                                }
                        )
                        colorModel.state = NEW
                    }
                }

        unsubscribeOnDestroy(disposable)
    }

    fun onScrolledToEnd(item: ColorItem?, itemCount: Int) {
        info("onScrolledToEnd $itemCount")
        launch(UI) {
            viewState.showProgressHistory(true)
            val date = item?.dateValue() ?: Date()
            val colors = dao.getLessDesc(date, itemCount.toLong(), LOAD_ITEM_PAGE_CNT)
            viewState.addColorHistoryToStart(colors)
            viewState.showProgressHistory(false)
        }
    }

    fun onNewColorClick(item: ColorItem) {
        colorModel.color = item
        router.onNewColorView(viewState)
    }
}