package com.a7glyphs.testtask.mvp.m.net

import com.a7glyphs.testtask.mvp.m.net.data.SrvAuth
import com.a7glyphs.testtask.mvp.m.net.data.SrvColor
import com.a7glyphs.testtask.mvp.m.net.data.SrvToken
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface Api7gDemo {
    companion object {
        val API_BASE_URL = "https://android-test.7g-demo.com"
    }


    @POST("/oauth/token")
    @Headers("Content-Type: application/json")
    fun refreshToken(@Body srvAuth: SrvAuth): Call<SrvToken>

    @GET("api/colors")
    fun getColors(): Call<List<SrvColor>>
}