package com.a7glyphs.testtask.mvp.p

import com.a7glyphs.testtask.App
import com.a7glyphs.testtask.R
import com.a7glyphs.testtask.mvp.m.ColorModel
import com.a7glyphs.testtask.mvp.m.NetError
import com.a7glyphs.testtask.mvp.m.Res
import com.a7glyphs.testtask.mvp.v.LoginView
import com.arellomobile.mvp.InjectViewState
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.info
import javax.inject.Inject


@InjectViewState
class LoginPresenter : BasePresenter<LoginView>() {
    init {
        App.appComponent.inject(this)
    }

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var model: ColorModel


    override fun onFirstViewAttach() {
        info("onFirstViewAttach")
        super.onFirstViewAttach()
/*
        //TODO dda: remove it before release and test
        dao.removeAll()
        dao.generateTestData(Date().add(Calendar.DAY_OF_MONTH, -200), Date().add(Calendar.DAY_OF_MONTH, -1))
        onLogin()
*/
    }

    fun onUserNameInput(userName: String?) {
        info("onUserNameInput: $userName")

        model.credential.userName = userName
        onInput()
    }

    fun onUserEmailInput(email: String?) {
        info("onUserEmailInput: $email")
        model.credential.email = email
        onInput()
    }

    fun onInput() {
        if (model.credential.isReadyForLogin())
            viewState.switchLoginPossible(true)
        else
            viewState.switchLoginPossible(false)

    }

    fun onLogin() {
        info("onLogin")
        launch(UI) {
            try {
                viewState.showProgress(true)
                val result = model.refreshToken()
                when (result) {
                    is Res.Ok -> {
                        router.onLogin(viewState)
                    }
                    is Res.Error ->
                        when (result.value) {
                            is NetError.HttpError -> viewState.showError(R.string.server_error)
                            is NetError.NetworkError -> viewState.showError(R.string.network_error)
                        }
                }

            } finally {
                viewState.showProgress(false)
            }
        }
    }
}
