package com.a7glyphs.testtask

import android.app.Application
import com.a7glyphs.testtask.di.AppComponent
import com.a7glyphs.testtask.di.DaggerAppComponent
import com.a7glyphs.testtask.di.module.CommonModule
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class App : Application(), AnkoLogger {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        info("onCreate")
        super.onCreate()
        appComponent = DaggerAppComponent
                .builder()
                .commonModule(CommonModule(this))
                .build()
        appComponent.inject(this)
    }

    override fun onTerminate() {
        info("onTerminate")
        super.onTerminate()
    }
}