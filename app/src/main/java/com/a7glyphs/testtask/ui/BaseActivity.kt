package com.a7glyphs.testtask.ui

import android.content.Intent
import com.a7glyphs.testtask.mvp.v.BaseView
import com.arellomobile.mvp.MvpAppCompatActivity
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info


abstract class BaseActivity : MvpAppCompatActivity(), AnkoLogger, BaseView {

    protected val jobMap: MutableMap<Int, Job> = HashMap()

    override fun onDestroy() {
        super.onDestroy()
        info("onDestroy jobMap: ${jobMap.keys}")
        jobMap.entries
                .map { it.value }
                .filter { it.isActive }
                .forEach { it.cancel() }
        jobMap.clear()
    }

    override fun runNextView(intent: Intent) {
        startActivity(intent)
    }

    override fun runNextViewForResult(intent: Intent) {
        startActivityForResult(intent, 0)
    }

    override fun backToPreviousView() {
        onBackPressed()
    }

    fun launchUI(jobId: Int, body: suspend CoroutineScope.() -> Unit) {
        info("launchUI jobId: $jobId")
        val job = launch(UI, CoroutineStart.DEFAULT, body)
        jobMap.put(jobId, job)
    }

    fun onJobFinish(jobId: Int) {
        //jobMap[jobId]?.cancel()
        jobMap.remove(jobId)
    }
}