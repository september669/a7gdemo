package com.a7glyphs.testtask.ui.extra

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.a7glyphs.testtask.R
import com.a7glyphs.testtask.mvp.m.ColorItem
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class RecycleAdapter(val dateFormat: SimpleDateFormat = SimpleDateFormat("dd", Locale.getDefault()),
                     val layoutManager: RecyclerView.LayoutManager,
                     val calcItemSize: (RecyclerView.LayoutManager) -> ItemSize
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger {
    companion object {
        val TYPE_STUB = 42424242
        val TYPE_ITEM = 424242421
    }

    data class ItemHolder<out T>(val item: T?, val isItem: Boolean)

    data class ItemSize(val width: Int, val height: Int)

    @LayoutRes
    var stubLayout: Int = -1

    var itemClickListener: ((item: ColorItem) -> Unit)? = null


    private val items: MutableList<ItemHolder<ColorItem>> = ArrayList()
    private var stubEndCnt: Int = 0
    private var stubStartCnt: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        //info("onCreateViewHolder viewType: $viewType")
        return if (viewType == TYPE_STUB) {
            val itemView = LayoutInflater.from(parent?.context).inflate(stubLayout, parent, false)
            HeaderFooterViewHolder(itemView, layoutManager.height)
        } else {
            val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.gallery_item, parent, false)
            RecycleViewHolder(itemView, layoutManager, itemClickListener, calcItemSize)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (items[position].isItem && holder is RecycleViewHolder) {
            //info("onBindViewHolder item")
            val item = items[position].item
            if (item != null) holder.setItem(item, dateFormat)
        } else {
            info("onBindViewHolder stub")
            //doNothing
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position].isItem)
            TYPE_ITEM
        else
            TYPE_STUB
    }

    override fun getItemCount(): Int {
        //info("getItemCount ${items.size}")
        return items.size
    }


    fun getRealItemCount(): Int {
        info("getItemCount ${items.size}")
        return items.size - stubEndCnt - stubStartCnt
    }

    fun getFirstRealItem(): ColorItem? {
        return if (items.size == 0) null else items[stubStartCnt].item
    }

    fun addItemToStart(itemsToAdd: Collection<ColorItem>) {
        val tmp = ArrayList<ItemHolder<ColorItem>>(itemsToAdd.size)
        itemsToAdd.forEach { tmp.add(ItemHolder(it, true)) }
        if (items.addAll(0, tmp)) {
            info("setItems ${items.size}")
            notifyDataSetChanged()
        }
    }

    fun addStubToEnd() {
        items.add(ItemHolder(null, false))
        stubEndCnt++
        info("addStubToEnd")
        notifyItemInserted(items.size - 1)
    }

    fun setItems(itemsToAdd: Collection<ColorItem>, notifyUi: Boolean) {
        val tmp = ArrayList<ItemHolder<ColorItem>>(itemsToAdd.size)
        itemsToAdd.forEach { tmp.add(ItemHolder(it, true)) }
        if (items.addAll(tmp)) {
            info("setItems ${items.size}")
            notifyDataSetChanged()
        }
    }

    fun mergeFirstItemFromEnd(newItem: ColorItem, comparator: (left: ColorItem?, right: ColorItem?) -> Boolean) {

        val id = items.indexOfFirst { comparator(it.item, newItem) }
        info("mergeFirstItemFromEnd newItem:${newItem.dateValue()} id: $id")
        if (id >= 0) {
            items[id] = ItemHolder(newItem, true)
            notifyItemChanged(id)
        } else if (items.size > 0) {
            val idStb = items.size - stubEndCnt
            items.add(idStb, ItemHolder(newItem, true))
            notifyItemInserted(idStb)
        } else {
            items.add(ItemHolder(newItem, true))
            notifyItemInserted(0)
        }
    }

    class HeaderFooterViewHolder(itemView: View, height: Int) : RecyclerView.ViewHolder(itemView) {
        init {
            val params = itemView.layoutParams
            params.height = height
            params.width = height
            itemView.layoutParams = params

        }
    }
}