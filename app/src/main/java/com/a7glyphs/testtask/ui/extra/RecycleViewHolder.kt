package com.a7glyphs.testtask.ui.extra

import android.content.res.ColorStateList
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import com.a7glyphs.testtask.R
import com.a7glyphs.testtask.dp
import com.a7glyphs.testtask.mvp.m.ColorItem
import org.jetbrains.anko.AnkoLogger
import java.text.SimpleDateFormat


class RecycleViewHolder(itemView: View?,
                        private val layout: RecyclerView.LayoutManager,
                        val itemClickListener: ((item: ColorItem) -> Unit)? = null,
                        private val calcItemSize: (RecyclerView.LayoutManager) -> RecycleAdapter.ItemSize)
    : RecyclerView.ViewHolder(itemView), AnkoLogger {


    private val imageButton: Button = itemView?.findViewById(R.id.imageButton)!!
    private val text: TextView = itemView?.findViewById(R.id.text)!!

    fun setItem(item: ColorItem, format: SimpleDateFormat) {
        itemView?.setOnClickListener {
            itemClickListener?.invoke(item)
        }
        imageButton.setOnClickListener {
            itemClickListener?.invoke(item)
        }
        text.setOnClickListener {
            itemClickListener?.invoke(item)
        }

        val (itemWidth, itemHeight) = calcItemSize(layout)

        val params = itemView.layoutParams
        params.height = itemHeight
        if (item.dateValue() == null) params.width = itemWidth
        itemView.layoutParams = params

/*
            val imgSize = (0.4 * layoutHeight).toInt()
            imageButton.layoutParams = RelativeLayout.LayoutParams(imgSize, imgSize)
*/

        val imgSize: Int
        if (item.dateValue() != null) {
            imgSize = (0.45 * itemHeight).toInt()
            text.textSize = (0.15 * itemHeight).toInt().dp.toFloat()
            text.visibility = VISIBLE
            text.text = format.format(item.dateValue())

        } else {
            imgSize = itemHeight
            //itemView.elevation = 10.px.toFloat()
            //imageButton.elevation = 10.px.toFloat()
            text.visibility = GONE
        }
        imageButton.backgroundTintList = ColorStateList.valueOf(item.colorValue())
        imageButton.layoutParams = RelativeLayout.LayoutParams(imgSize, imgSize)
        itemView.invalidate()
    }
}