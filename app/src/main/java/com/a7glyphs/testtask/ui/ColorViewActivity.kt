package com.a7glyphs.testtask.ui

import android.os.Bundle
import android.support.annotation.ColorInt
import com.a7glyphs.testtask.R
import com.a7glyphs.testtask.colorFromId
import com.a7glyphs.testtask.mvp.m.ColorItem
import com.a7glyphs.testtask.mvp.p.ColorViewPresenter
import com.a7glyphs.testtask.mvp.v.ColorView
import com.a7glyphs.testtask.toHsv
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_color_view.*

class ColorViewActivity : BaseActivity(), ColorView {

    @InjectPresenter
    lateinit var presenter: ColorViewPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_color_view)

        btnCancel.setOnClickListener({
            presenter.onColorCancel()
        })

        btnOk.setOnClickListener({
            presenter.onColorApprove()
        })
    }

    override fun showColor(color: ColorItem) {
        rootLayout.setBackgroundColor(color.colorValue())
        labelSelectColor.text = color.nameValue()
        txtSelectedColorHex.text = color.stringHexValue()

        val toHsv = color.colorValue().toHsv()
        @ColorInt
        val textColor =
                if (toHsv[2] < 0.8) //TODO dda: need test value or discuss about it
                    R.color.colorTextLabelPrimary.colorFromId(baseContext)
                else
                    R.color.colorTextLabelPrimaryDark.colorFromId(baseContext)
        labelSelectColor.setTextColor(textColor)
        txtSelectedColorHex.setTextColor(textColor)
        btnCancel.setTextColor(textColor)
    }

    override fun showProgress(visible: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
