package com.a7glyphs.testtask.ui

import android.os.Bundle
import android.support.annotation.StringRes
import com.a7glyphs.testtask.R
import com.a7glyphs.testtask.afterTextChanged
import com.a7glyphs.testtask.colorFromId
import com.a7glyphs.testtask.mvp.p.LoginPresenter
import com.a7glyphs.testtask.mvp.v.LoginView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.dd.processbutton.iml.ActionProcessButton
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.experimental.delay
import org.jetbrains.anko.info
import java.util.concurrent.TimeUnit


/**
 * A login screen that offers login via email/email.
 */
class LoginActivity : BaseActivity(), LoginView {
    companion object {
        val JOB_SHOW_ERROR = 0
    }

    @InjectPresenter
    lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        info("onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.text = getString(R.string.log_in)

        btnLogin.setBackgroundColor(R.color.colorLoginBtn.colorFromId(baseContext))
        btnLogin.setOnClickListener({ loginPresenter.onLogin() })
        btnLogin.setMode(ActionProcessButton.Mode.ENDLESS)


        txtLogin.afterTextChanged { loginPresenter.onUserNameInput(it) }
        txtEmail.afterTextChanged { loginPresenter.onUserEmailInput(it) }
        txtEmail.setText("test@7glyphs.com")
    }

    override fun onPause() {
        info("onPause")
        super.onPause()
    }

    override fun onDestroy() {
        info("onDestroy")
        super.onDestroy()
    }

    override fun showProgress(visible: Boolean) {
        info("showProgress $visible")
        btnLogin.progress = if (visible) 1 else 0
        txtLogin.isEnabled = !visible
        txtEmail.isEnabled = !visible
        btnLogin.isEnabled = !visible
    }

    override fun switchLoginPossible(isPossible: Boolean) {
        info("switchLoginPossible: $isPossible")
        btnLogin.isEnabled = isPossible
        val color =
                if (isPossible) R.color.colorTextLabelPrimary.colorFromId(this)
                else R.color.colorTextLabelPrimaryTrans.colorFromId(this)
        btnLogin.setTextColor(color)
    }

    override fun showError(@StringRes string: Int) {
        launchUI(JOB_SHOW_ERROR, {
            btnLogin.text = getString(string)
            delay(3, TimeUnit.SECONDS)
            btnLogin.text = getString(R.string.log_in)
            onJobFinish(JOB_SHOW_ERROR)
        })
    }
}
