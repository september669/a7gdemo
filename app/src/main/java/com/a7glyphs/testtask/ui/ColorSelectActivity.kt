package com.a7glyphs.testtask.ui

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewTreeObserver
import com.a7glyphs.testtask.R
import com.a7glyphs.testtask.mvp.m.ColorItem
import com.a7glyphs.testtask.mvp.m.db.data.DaoColor
import com.a7glyphs.testtask.mvp.p.ColorSelectPresenter
import com.a7glyphs.testtask.mvp.v.SelectColorView
import com.a7glyphs.testtask.ui.extra.RecycleAdapter
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_select.*
import org.jetbrains.anko.info
import java.text.SimpleDateFormat
import java.util.*

class ColorSelectActivity : BaseActivity(), SelectColorView {
    companion object {

        val DATE_FORMAT_DAY: SimpleDateFormat = SimpleDateFormat("dd", Locale.getDefault())
        val DATE_FORMAT_DAY_MONTH: SimpleDateFormat = SimpleDateFormat("dd MMMM", Locale.getDefault())
    }

    @InjectPresenter
    lateinit var presenter: ColorSelectPresenter

    lateinit var listColorHistory: RecyclerView

    lateinit var listColorNew: RecyclerView

    lateinit var adapterColorHistory: RecycleAdapter

    lateinit var adapterColorNew: RecycleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        info("onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select)



        listColorHistory = historyList
        val llmHistory = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        llmHistory.stackFromEnd = true
        listColorHistory.layoutManager = llmHistory
        adapterColorHistory = RecycleAdapter(DATE_FORMAT_DAY, listColorHistory.layoutManager, calcItemSize = {
            RecycleAdapter.ItemSize(it.height, it.height)
        })
        adapterColorHistory.stubLayout = R.layout.gallery_item_stub
        listColorHistory.adapter = adapterColorHistory
        listColorHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //info("onScrolled to ${llmHistory.findFirstVisibleItemPosition()}")
                //info("onScrolled comp to ${llmHistory.findFirstCompletelyVisibleItemPosition()}")
                if (llmHistory.findFirstCompletelyVisibleItemPosition() == 0) {
                    val realItemCnt = adapterColorHistory.getRealItemCount()
                    val realItem = adapterColorHistory.getFirstRealItem()
                    presenter.onScrolledToEnd(realItem, realItemCnt)
                }
            }
        })

        val spanCnt = 3
        listColorNew = colorsList
        listColorNew.layoutManager = GridLayoutManager(this, spanCnt, LinearLayoutManager.HORIZONTAL, false)
        adapterColorNew = RecycleAdapter(DATE_FORMAT_DAY, listColorNew.layoutManager, calcItemSize = {
            RecycleAdapter.ItemSize(it.height / 3, it.height / 3)
        })
        listColorNew.adapter = adapterColorNew
        adapterColorNew.itemClickListener = { presenter.onNewColorClick(it) }
    }

    override fun onResume() {
        super.onResume()
        info("onResume")
        presenter.onViewResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        info("onDestroy")
    }

    override fun showProgress(visible: Boolean) {
        info("showProgress $visible")
        progressBar.visibility = if (visible) VISIBLE else GONE
    }

    override fun showProgressHistory(visible: Boolean) {
        info("showProgressHistory $visible")
        progressBarHistory.visibility = if (visible) VISIBLE else GONE
    }

    override fun setHeader(date: Date) {
        info("setHeader$date")
        labelHeader.text = DATE_FORMAT_DAY_MONTH.format(date)
    }

    override fun addColorHistoryToStart(colors: List<DaoColor>) {
        info("addColorHistoryToStart ${colors.size}")
        if (colors.isNotEmpty()) {
            adapterColorHistory.addItemToStart(colors.reversed())
            listColorHistory.scrollAfterAdded(colors.size)
        }
    }

    override fun showColorHistory(colors: List<DaoColor>) {
        info("showColorHistory ${colors.size}")
        adapterColorHistory.setItems(colors.reversed(), false)
        listColorHistory.doAfterRecycleAdded(body = {
            val height = listColorHistory.layoutManager.height
            val cnt =
                    if (height > 0) listColorHistory.layoutManager.width / listColorHistory.layoutManager.height / 2
                    else 1
            info("showColorHistory add stubs cnt: $cnt")
            for (i in 1..cnt) {
                adapterColorHistory.addStubToEnd()
            }
            listColorHistory.scrollAfterAdded(adapterColorHistory.itemCount - 1)
        })
    }

    override fun showColors(colors: List<ColorItem>) {
        info("showColors ${colors.size}")
        adapterColorNew.setItems(colors, true)
    }

    override fun setTodayColor(color: ColorItem, comparator: (left: ColorItem?, right: ColorItem?) -> Boolean) {
        info("setTodayColor ${color.dateValue()}")
        adapterColorHistory.mergeFirstItemFromEnd(color, comparator)
        listColorHistory.scrollAfterAdded(adapterColorHistory.itemCount - 1)
    }

    private fun RecyclerView.scrollAfterAdded(position: Int) {
        this.doAfterRecycleAdded {
            info("scrollAfterAdded $position")
            this.scrollToPosition(position)
        }
    }

    private fun RecyclerView.doAfterRecycleAdded(body: () -> Unit) {
        this.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                listColorHistory.viewTreeObserver.removeOnGlobalLayoutListener(this)
                body()
            }
        })
    }
}
